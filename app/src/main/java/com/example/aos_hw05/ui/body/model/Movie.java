package com.example.aos_hw05.ui.body.model;

public class Movie {
      private  int hero_image;
    private String movieName;

    public Movie(int hero_image, String movieName){
        this.hero_image = hero_image;
         this.movieName = movieName;
    }
    public int getHeroImage() {

        return hero_image;
    }
    public String getMovieName() {

        return movieName;
    }
}