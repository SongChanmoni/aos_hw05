package com.example.aos_hw05.ui.body.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aos_hw05.ui.body.model.Movie;
import com.example.aos_hw05.ui.body.model.Category;
import com.example.aos_hw05.R;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {
    private ArrayList<Category> categoryArrayList;
    public Context cxt;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView category;
        public RecyclerView childRecyclerView;

        public MyViewHolder(View itemView) {
            super(itemView);

            category = itemView.findViewById(R.id.Movie_category);
            childRecyclerView = itemView.findViewById(R.id.Child_RV);
        }
    }

    public CategoryAdapter(ArrayList<Category> exampleList, Context context) {
        this.categoryArrayList = exampleList;
        this.cxt = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.parent_recyclerview_items, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return categoryArrayList.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Category currentItem = categoryArrayList.get(position);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(cxt, LinearLayoutManager.HORIZONTAL, false);
        holder.childRecyclerView.setLayoutManager(layoutManager);
        holder.childRecyclerView.setHasFixedSize(true);

        holder.category.setText(currentItem.movieCategory());
        ArrayList<Movie> arrayList = new ArrayList<>();


        if (categoryArrayList.get(position).movieCategory().equals("Popular on Netflix")) {
            arrayList.add(new Movie(R.drawable.drama1,"Drama Movie"));
            arrayList.add(new Movie(R.drawable.drama5,"Drama Movie"));
            arrayList.add(new Movie( R.drawable.drama4,"Drama Movie"));
            arrayList.add(new Movie( R.drawable.drama3,"Drama Movie"));
            arrayList.add(new Movie( R.drawable.drama2,"Drama Movie"));

        }

        if (categoryArrayList.get(position).movieCategory().equals("Trending Now")) {
            arrayList.add(new Movie( R.drawable.avengers_endgame1,"Avengers Endgame"));
            arrayList.add(new Movie(R.drawable.avengers_endgame6,"Avengers Endgame"));
            arrayList.add(new Movie( R.drawable.avengers_endgame2,"Avengers Endgame"));
            arrayList.add(new Movie(R.drawable.avengers_endgame4,"Avengers Endgame"));
            arrayList.add(new Movie( R.drawable.avengers_endgame6,"Avengers Endgame"));

        }

        if (categoryArrayList.get(position).movieCategory().equals("Watch it Again")) {
            arrayList.add(new Movie(R.drawable.netflix2,"Netflix TV"));
            arrayList.add(new Movie(R.drawable.netflix3,"Netflix TV"));
            arrayList.add(new Movie( R.drawable.netflix4,"Netflix TV"));
            arrayList.add(new Movie( R.drawable.netflix1,"Netflix TV"));
            arrayList.add(new Movie( R.drawable.netflix5,"Netflix TV"));

        }

        if (categoryArrayList.get(position).movieCategory().equals("TV Series")) {
            arrayList.add(new Movie( R.drawable.moive14,"Hollywood Movie"));
            arrayList.add(new Movie(R.drawable.moive16, " Hollywood Movie"));
            arrayList.add(new Movie( R.drawable.moive12,"Hollywood Movie"));
            arrayList.add(new Movie(R.drawable.moive15, " Hollywood Movie"));
            arrayList.add(new Movie( R.drawable.moive13,"Hollywood Movie"));


        }

        CustomAdapter customAdapter = new CustomAdapter(arrayList,holder.childRecyclerView.getContext());
            holder.childRecyclerView.setAdapter(customAdapter);
        }

}