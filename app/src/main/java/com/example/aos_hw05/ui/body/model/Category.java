package com.example.aos_hw05.ui.body.model;

public class Category {
    private String movieCategory;


    public Category(String movieCategory) {

        this.movieCategory = movieCategory;
    }
    public String movieCategory() {

        return movieCategory;
    }

}