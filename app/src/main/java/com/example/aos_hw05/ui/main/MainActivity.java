package com.example.aos_hw05.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;


import com.example.aos_hw05.R;
import com.example.aos_hw05.ui.body.Adapters.CategoryAdapter;
import com.example.aos_hw05.ui.body.model.Category;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView parentRecyclerView;
    private RecyclerView.Adapter ParentAdapter;
    ArrayList<Category> categoryArrayList = new ArrayList<>();
    private RecyclerView.LayoutManager parentLayoutManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);



        categoryArrayList.add(new Category("Popular on Netflix"));
        categoryArrayList.add(new Category("Trending Now"));
        categoryArrayList.add(new Category("Watch it Again"));
        categoryArrayList.add(new Category("TV Series"));

        parentRecyclerView = findViewById(R.id.Parent_recyclerView);
        parentRecyclerView.setHasFixedSize(true);
        parentLayoutManager = new LinearLayoutManager(this);
        ParentAdapter = new CategoryAdapter(categoryArrayList, MainActivity.this);
        parentRecyclerView.setLayoutManager(parentLayoutManager);
        parentRecyclerView.setAdapter(ParentAdapter);
        ParentAdapter.notifyDataSetChanged();


    }
}